#!/bin/bash

INPUT="$1"
if [ -z "${INPUT}" ] ; then
  echo "generate_subtitles.sh [video-file]" >&2
  exit 1
fi

pipenv run auto_subtitle --language en \
  --task transcribe \
  --output_dir "${PWD}" \
  --output_srt True \
  --srt_only True \
  --verbose True \
  "${INPUT}"
