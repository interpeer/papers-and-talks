# Papers and Talks

This repo tracks papers and conference talks, and links to the final results
(if any).

## Conferences

We're tracking conferences in the [conferences folder](./conferences/).
Entries start with the conference year (date, if necessary), followed by the
conference title. For now, this simple scheme seems to work. A `conference.md`
file in each folder should ocntain the full title and a link to the website,
and the schedule entry and/or video where possible. Enough so that people can
browse past stuff.

- Submitted but incomplete entries simply live in that folder.
- Completed conferences get moved to a `done` folder.
  - Videos are uploaded to [a public NextCloud folder](https://nextcloud.interpeer.io/s/KHxjFxBDzFTeoa4),
    and [available on PeerTube](https://makertube.net/c/interpeer_talks/videos)
- Declined proposals get moved to a `declined` folder.

Keeping things relatively simple that way.

## (Published) Papers

- [Peer-to-Peer Media Storage and Casting](https://codeberg.org/interpeer/p2p-storage-casting)

## TODO

| **Conference** | **Submission deadline** | **Topics** |
|----------------|-------------------------|------------|
| TODO | ||
