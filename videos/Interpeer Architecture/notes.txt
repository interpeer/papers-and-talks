Other Architectures
- Peer-to-Peer Networks
  - venn diagram p2p vs. file sharing and how everything focuses on the narrow
    overlap (slide 1)
  - components:
    - overlay:
      - over UDP for NAT piercing
      - TURN (rfc!)
    - NAT piercing
      - STUN (rfc!)
    - mechanism for finding peers (public IP:port from NAT-pierced connections)
    - mechanism for finding content
  - Overlays
    - similar to IP or UDP
      - peer node identifier, *maybe* something like a port
  - DHT:
    - "routing", in the sense of advertising/receiving info about reachability
      mapping peer node ID to "socket" (see previous slide)
    - possibly routing in the sense of using a TURN type approach to forward,
      or TURN, or direct p2p messaging
    level protocols
  - content distribution:
    - using the same overlay to request content chunks and return them
    -> JUST FILE SHARING
  - not *architecturally* much different
    - same hourglass with "messaging" at the IP/UDP level
    - not at all given that any implementation has clearly defined layers here
- Information-Centric Networking (CCN, NDN):
  - interest/data at pivot point
  - some kind of transport below (in practice: something IP based)
  - interest/data is like application protocols: pull oriented
    - but each data fragment is independent
    - can route every fragment separately!!!
  - application layer(s) different, though there are experiments with e.g. HTTP over ICN,
    etc.
  - interest that results in larger response is not really part of the protocol, e.g.
    "request a stream"
  - fundamentally unidirectional; reflexive forwarding
- Delay-Tolerant Networking (DTN)
  - Bundle Protocol at pivot point
  - "convergence layers" below
  - BP is *push* oriented, _but_ with transfer of custodianship (as opposed to
    resend)
    - can route every bundle separately, except may not wish to -> custodianship is a
      fairly restricted thing
  - minus real-time expectations, it's easily possible to e.g. layer HTTP over it
    - authentication & authorization with short timeouts is difficult

Analyze Push vs Pull
- Push is very much communications channel oriented. Messaging, notifications, streaming, monitoring,
  command & control.  The destination is *known*. It's more primitive, in a way, so more suited to
  lower layer protocols.
- Pull is very much need oriented. I need some info or some result. The destination is less important
  than the data, and may be unknown. It's more abstract, so more suited to application layer protocols.
- Because the destination in pull scenarios is less important, this allows for powerful
  routing decisions, based on the need and situation rather than relatively fixed network
  structure.
- BUT pretty difficult to get any kind of custodianship transfer done when you can't
  ensure the next hop, so some control is desirable with intermittency.
- Pull isn't *wrong* - usually it only makes sense to send data to parties that are interested.
- Push isn't *wrong* - it's sometimes necessary to just send data, and let the receiver decide if
  they're interested.

Can we get the best of both worlds?
- What are *user* needs? Users want to decide what they're interested in (pull), but also want
  to be notified (push) of interesting things happening. -> publish/subscribe
  - publish (pull) what you're interested in
  - receive responses without further invitation (push from the remote end)
  - unsubscribe
  - We can treat individual fragment pulling as a very tightly scoped pubsub operation, where
    the unsubscription occurs immediately after the fragment was sent
- What about custodiandship transfer? This is a push-side operation, under sender's control
  -> can be requested in the subscription, though
- Groups:
  - analyze how "web scale" is a myth, because most groups are not for tens of thousands
  - telegram:
    - channel is one to many, unlimited
    - broadcast group is one to many, unlimited
    - group has maximum cap of 200,000 (interactive)
      - mostly lurking and inactive
