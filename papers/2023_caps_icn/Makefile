.PHONY: all clean dist print figures

AASVG_IN = $(wildcard figures/*.txt)
AASVG_SVG = $(foreach fname,$(AASVG_IN),$(basename $(fname)).svg)
AASVG_PDF = $(foreach fname,$(AASVG_IN),$(basename $(fname)).pdf)

P = caps_icn

default : $P.pdf

$P.dvi	: $(wildcard sections/*.tex *.tex *.bib figures/*.eps) figures
	latex  $P < /dev/null || $(RM) $@
	bibtex $P < /dev/null || $(RM) $@
	latex  $P < /dev/null || $(RM) $@
	latex  $P < /dev/null || $(RM) $@

$P.pdf  : $(wildcard sections/*.tex *.tex *.bib figures/*) figures
	pdflatex -draftmode -shell-escape $P
	bibtex $P
	pdflatex -draftmode -shell-escape $P
	pdflatex -shell-escape $P

$P.ps	: $P.dvi figures
	dvips -tletter -Ppdf $P.dvi -o $P.ps

$P.ps.gz: $P.ps
	$(RM) $P.ps.gz
	gzip -9 < $P.ps > $P.ps.gz

print:	$P.ps

dist:	$P.ps.gz

figures/%.svg: figures/%.txt
	@aasvg --spaces=2 <$< >$@

figures/%.pdf: figures/%.svg
	@convert $^ $@

figures: $(AASVG_PDF)

clean:
	$(RM) $P.log $P.aux $P.bbl $P.blg $P.out $P.dvi $P.ps $P.ps.gz texput.log
