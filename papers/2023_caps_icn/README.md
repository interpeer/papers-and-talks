# Capabilities for Distributed Authorization in Information-Centric Networking

- Submitted to [ICN 2023](https://conferences.sigcomm.org/acm-icn/2023/), but declined.
- 2023-08-08: [Preprint submitted to arxiv](https://arxiv.org/abs/2308.04140)
- 2023-08-09: [Preprint submitted to osf.io](10.31219/osf.io/tkxfr).

The paper was rejected for ICN 2023, with [useful reviews](./icn23-reviews-1.md).

The good news is: this feedback is easily addressed. The bad news is that
addressing it would break the page limit, so this is more likely to go into
[the draft](https://datatracker.ietf.org/doc/draft-jfinkhaeuser-caps-for-distributed-auth/) (
See also https://specs.interpeer.io for working versions).

