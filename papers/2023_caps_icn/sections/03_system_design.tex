\section{System Design}
\label{sec:system}

When comparing the related work, it becomes apparent that it is difficult to
address authorization without first discussing authentication (section
\ref{sec:system:authentication}); to make matters
worse, authentication itself can refer to subtly different things. This paper
therefore first summarizes authentication as understood in the context of the
proposed \Cap/ scheme (section \ref{sec:system:authentication}).

Section \ref{sec:system:authorization} examines different phases of authorization in order to both
better localize how related work centralizes the authorization problem, but
also to provide the conceptual clarity required to design a distributed
authorization scheme.

Sections \ref{sec:system:validity} and \ref{sec:system:revocation} address ways
to mitigate potentially compromised keys referenced in \Caps/.

Finally, section \ref{sec:system:components} lists necessary components of a
\Cap/ in order to address all of the preceding considerations.

\subsection{(Distributed) Authentication}
\label{sec:system:authentication}

Authentication is essentially an atomic operation yielding a binary
response to the question: "Is this \Subject/ who they claim they are?"

However, the \Subject/ can be either of a real-world \Person/ or entity\footnote{
We can use "\Person/" similar to the legal sense of either a natural
person (human) or a legal person, or some other meaningful group of natural
persons.}, or it can refer to a digital \Agent/ acting on behalf of a \Person/.

This difference requires two distinct authentication processes.

\subsubsection{Authentication of a \Person/}

Authentication a \Person/ establishes their identity by comparing the
\Person/'s observable characteristics to previously established records;
thus, if e.g. their fingerprints match those on Alice's record, they are
identified as Alice.

\subsubsection{Authentication of an \Agent/}

\Agent/ identification revolves around verifying that the \Agent/ is in
possession of some secret, such as via \CredAuth/ or \CryptAuth/.

In either case, a suitable challenge/response protocol establishes whether
the \Agent/ can prove such possession.

\subsubsection{Full Trust Chain}

In order to establish that an \Agent/ acts on behalf of a \Person/, some means
of combining the two approaches above is required. In the digital realm,
observing physical \Person/ characteristic is possible by some biometric
scanning process. But in distributed systems, the \Person/ is rarely in
physical proximity of the authenticating device, requiring that authentication
becomes distributed in some way.

The method of choice, such as in TLS's use of X.509 certificates, is to
accept that there is a delay between \Person/ and \Agent/ authentication.

\Person/ authentication occurs beforehand, and endows an \Agent/ \Id/
with verifiable metadata that sufficiently proves the \Person/'s identity
for the use case. For this reason, we refer to this as \Endowment/\footnote{
In practice, automated certificate
signing via protocols such as ACME\cite{rfc8555} skip the identification of
real-world relationships, and instead focus only on whether related services
are under the requester's control. This may be deemed sufficient in some use
cases.}.

A trusted authority performs use-case relevant \Person/ authentication. It
produces a set of identifying metadata, and adds the \Agent/ \Id/. It then
notarizes the combination by adding it's cryptographic signature, producing
a cryptographic certificate.

At the point of its own authentication, the \Agent/ an then presents the
certificate along with answering the cryptographic challenge. For the trust
chain to be established, all that is required is that the \Agent/ \Id/ in
the certificate is the same as used for the challenge/response protocol (or
one can be verifiably mapped to the other).

Conceptually, this introduces a \TDTC/: between the creation of a certificate,
and the communication of it to an endpoint that needs to authenticate the
\Agent/, sufficient time passes that authentication cannot be viewed as an
atomic unit any longer.

This concept of a \TDTC/ can similarly be applied to the problem of
authorization, as described in section \ref{sec:system:authorization}.

\subsection{Distributed Authorization}
\label{sec:system:authorization}

\begin{figure*}[tp]
  \centering
  \includegraphics[width=\linewidth]{figures/architecture}
  \caption{Architecture for capability-based distributed authorization}
  \label{fig:architecture}
\end{figure*}

In the proposed scheme, we divide the authorization process into two distinct
phases similar to the preceding analysis of distributed authentication: a
pre-authorization phase grants some privilege, and serializes it into a \Cap/.
The \Cap/ propagates to the second phase via a \TDTC/. In the second
verification phase, the \Cap/ contents are verified in a manner comparable to
that performed in authentication (section \ref{sec:system:authentication}).

Figure \ref{fig:architecture} illustrates the phases below, the components
involved and
communication flows between them.

\subsubsection{Phase I: Pre-Authorization}

The pre-authorization phase is initiated by an \AuthQuery/. A \Grantor/ with
authority to grant privileges looks up \AuthTuples/ in an \AuthStore/.

Having found an appropriate \Grant/, the \Grantor/ adds required metadata,
serializes the results and adds a signature, creating a \Cap/.

The \Cap/ enters the conceptual \TDTC/.

\subsubsection{Phase II: Verification}

The verification phase starts with an \AccessReq/ to a \Service/. The request should come from
the \Grantee/ of a \Cap/ and should include the \Cap/ in its payload.

  Strictly
speaking, the \TDTC/ can have delivered the \Cap/ independently to the
\Service/ before the request was made. However, as the purpose of capabilities
is to embed all relevant information to authorize a request, it makes sense to
treat the \Cap/ as part of the request payload. The essential part is that the
request and \Cap/ are both present at the \Service/ at this point.

It is at this point assumed that the \Agent/ is authenticated to represent the
\Grantee/, otherwise the request must fail.

The \Service/ then forwards the \Cap/ to a \Verifier/. The distinction between
\Service/ and \Verifier/ here is made purely in the functional domain; this
does not imply that they exist on separate network nodes. In fact, separating
them into distinct nodes might re-introduce a single point of failure for the
service.

The key point is that the \Service/ does not need to be aware of trust
chains, or be capable of cryptographic operations. It treats the \Verifier/ as
a function yielding a binary response: is the \Grantee/ authorized to access a
given object?

Once the \Verifier/ provides its response, the \Service/ can evaluate whether
it knows the \Object/ and supports the requested \Priv/. If that is the case,
it can grant effective access by responding to the request.

\subsection{Authorization Tuples}
\label{sec:system:tuples}

Given the above scheme, it is necessary to define the \AuthTuple/ that the
\Grantor/ looks up and signs.

The work previously done on \Caps/ is informative here (section
\ref{sec:related:ocap}): whereas OCAP defines merely a \Priv/ and an \Object/,
ICAP expands this to include a \Grantee/ as well in order to mitigate against
impersonators.

Given that the \Agent/ needs to authenticate as acting on behalf of the
\Grantee/, the ideal choice for the \Grantee/ field is either a public key that
the \Agent/ can use to authenticate, or a fingerprint or other identifier that
the \Service/ can easily map to such a public key.

Note, however, that a resolution step may introduce new system requirements that are most easily
addressed by centralized services, so care should be taken to avoid this
pitfall.

The \Object/ can be any application defined identifier. However, if the
\Object/ identifier namespace includes the \Grantee/ identifier namespace, this
opens possibilities for issuing \Caps/ on other \Persons/ in the system, which
can be used in more complex privilege management scenarios.

Similarly, the \Priv/ can be any application defined value. Existing privilege
schemes such as read/write permissions for file-like objects can be trivially
mapped into this \Cap/ scheme. However, more complex \Privs/ might include
specifying how to delegate certain \Privs/ to a different \Person/, and
establish a power of attorney scheme.

More discussion on delegation and \Persons/ as \Objects/ are outside the
scope of this paper.

Note also that these three fields can be
mapped neatly onto Resource Description Framework (RDF) N-Triples\cite{w3cRDF},
which opens the possibility of treating each field as a pointer to richer
documents. Again, however, additional resolution steps may re-centralize a
system making such a choice.

\subsection{Validity Period}
\label{sec:system:validity}

One concern with issuing storable and transmittable \Caps/ for authorization is
that a capability can be issued at a time when trust was established, but used
at a later date when trust was already lost.

Much the same issue exists in digital certificates for authentication, however,
so applying the same solution is valid: \Caps/ should include a pair of
timestamps defining the period of their validity.

Some use cases may require that validity periods are not used, however. This is
in particular the case when devices cannot have synchronized their local clock
yet, such as in e.g. Internet-of-Things (IoT) provisioning.

\subsection{Revocation}
\label{sec:system:revocation}

Issuing \Caps/ is a relatively cheap process. For this reason, validity periods
can usually be kept quite short.

However, it is always possible for trust to be lost within the validity period.
If there is then a way for the \Grantor/ to contact a \Service/ about impending
danger, that possibility should be explored.

Luckily, the data required for authoritatively granting privileges and for
authoritatively revoking them is essentially the same. It is recommended that
\Caps/ come in two flavors: grants and revocations, indicated by some flag in
their content.

In doing so, revocations similarly do not require the use of a third service
that can become a single point of failure. However, revocations must occur in
real-time, introducing such a service is unavoidable.

Adding revocations also introduces an ordering issue. Assume a grant C1 is
issued for a period of [S1, E1], then a revocation C2 is issued for [S2, E2].
Finally, a third \Cap/ C3 is issued, granting the privilege again for [S3, E3].

For simplicity's sake, assume S1 < S2 < S3, and E1 > E2 > E3; that is, the
periods are neatly nested into each other rather than overlapping.

For a given time point T, where T > S3 and T < E3, it is only possible to
determine whether the privilege is granted if the three \Caps/ are consulted in
issue order. If the consultation order were C1, C3 and C2, then C3's grant
would merely confirm C1's grant for time point, while C2's revocation would
yield the false final result.

Introducing revocations therefore introduces the need for some way to determine
the issue order of
\Caps/ -- X.509 for this purpose contains a \textsf{CertificateSerialNumber}
that issuers must strictly increment. A \Cap/ based distributed authorization
scheme should do the same.

\subsection{Capability Components}
\label{sec:system:components}

With the overall system design and additional considerations outlined in the
previous sections, it is now possible to provide a complete list of components
for \Caps/ that enable distributed authorization schemes.

\begin{description}
  \item[Grantor Identifier] \hfill \\ Due to the fact that cryptographic
    signatures are used, it is necessary to know which grantor issued a \Cap/,
    so that the signature can be verified using the appropriate key. The
    identifier here can be a full public key, or a fingerprint, or some other
    identifier that can be resolved to a public key
    (section \ref{sec:system:authorization}).
  \item[Flavour] \hfill \\ The \Cap/ can come in a grant flavor or a
    revocation flavor (section \ref{sec:system:revocation}).
  \item[Validity Period] \hfill \\ A period of validity should be provided
    using two timestamps (section \ref{sec:system:validity}).
  \item[Serial Number] \hfill \\ In order to avoid ambiguity, \Caps/ require a
    serial number (section \ref{sec:system:revocation}).
  \item[Authorization Tuple] \hfill \\ A \Cap/ must contain at least one such
    tuple. It is
    possible, however, for a \Cap/ to contain multiple \AuthTuples/ as an
    optimization (section \ref{sec:system:tuples}).
  \item[Signature] \hfill \\ Finally, a signature by the \Grantor/ over the
    preceding fields is required.
\end{description}
