\section{Related Work}
\label{sec:related}

This paper combines developments from \textit{object capabilities},
\textit{cryptographic certificates} and \textit{authorization protocols}.
Most related work falls into one of those three categories. Other informative
sources revolve around \textit{distributed identifiers}.

\subsection{Object Capabilities}
\label{sec:related:ocap}

Object capabilities\cite{OCAP} are first described in a 1966 paper as a
combination of a pointer to an object, and actions that may be carried out on
that object. A key property of capabilities is that they are serializable, that
is, one process can provide them to another as part of a request for some
computation. The paper imagines a \textit{supervisor} in a multi-user machine
to arbitrate access to objects based on the capabilities a process holds.

While the paper refers to \textit{principals} as the users or
groups on whose behalf computations are performed, they are not part of the
serialized capability. Instead, the system state provides answers: if a process requests some computation
presenting a capability, it is inferred that the user who owns the process is
the principal.

Many object capability based systems are designed and explored in the meantime,
but tend to remain restricted to single domains. In 1989, Gong recognizes
that they do not translate well to distributed systems, as network user
identifiers do not necessarily map to the same local user identifier on each of
the participating nodes.

ICAP addresses this
omission\cite{ICAP}, by adding the network user identifier to the transmitted
capability. It should be noted,
however, that the \textit{access control server} is not completely removed and
remains a single point of failure in the ICAP design.

\subsection{Cryptographic Certificates}
\label{sec:related:certs}

The more popular sibling of authorization is authentication; after all, one
needs to establish an identity of sorts before deciding whether to grant them
access to some object.

Two technologies in widespread use merit mention as related work: first, X.509
certificates\cite{X509} (used in e.g. Transport Layer Security) are probably
the most popular technology that aids in identification.

The certificates contain structured metadata about an entity that, one hopes,
can be used to establish a real-world identity. An additional piece of metadata
is a cryptographic fingerprint of the public part of an asymmetric key pair.
All of this is signed using the key of some chosen authority, establishing a
\textit{security assertion}: if one can trust the authority, then one can also
trust that the identified public key belongs to the identified real-world
entity.

This shifts the issue of establishing a root in a trust chain to the distribution
of authorities, or \textit{root certificates}. In principle, applications can
choose their own root certificates. But in practice, at least as far as the world
wide web is concerned, the most commonly used root certificates are distributed as
part of operating system updates.

Where the use of X.509 certificates provides a binary answer to a trust
question, Pretty Good Privacy (PGP) provides slightly more variations\cite{rfc4880}.
In much the same way as X.509, PGP associates some identifying metadata with a
public key. Unlike X.509, however, PGP does not rely on well-known roots of
trust.

Instead, PGP opts to let the recipient of a certificate decide
whether or not to trust the signer -- and by how much. For the latter purpose,
PGP adds a piece of metadata, a numeric \textit{trust level}, which users can choose
to assign their own, locally applicable meaning to.

In this manner, PGP crosses over into the realm of authorization: by assigning
a trust level of X to a key, and then deciding to grant some privilege to all
keys with that trust level, a user effectively authorizes the affected keys.

Trust levels, however, have only locally assigned meaning. While a user may
receive a recommendation to trust a key at a certain level, both the decision
to do so, as well as the interpretation as to what that means in practice,
remains entirely under their own control. It is therefore not possible to
grant a privilege, and generate a token encoding this \Grant/ in a way that
becomes an authoritative statement.

\subsection{Authorization Protocols}
\label{sec:related:auth}

In lieu of listing the abundance of different authorization protocols in use, this paper
instead focuses only on OAuth 2.0\cite{rfc6749} as a particularly meaningful
stand-in for the class of centralized authorization protocols due to its prevalence in web
applications. Authorization flows in OAuth all require an \AuthServer/. This is in part due
to the fact that the server is also intended to authenticate clients.

OAuth generates authorization tokens that encode a \Grant/; these may be
transmitted similar to \Caps/ as part of an access request. The specific format
of token is undefined in the base specification, however. Additional
specifications\cite{rfc6750} define how such bearer tokens may be transmitted.

It is worth noting that here the specifications introduce a requirement for TLS
transport encryption, in order to mitigate against various attacks involving
the leakage of tokens. The
conclusion must then be that the specification does not expect the token to
contain any protections such as \Caps/ in ICAP above that make copying them free
of risk.

One token format that can be used is JSON Web Token (JWT)\cite{rfc7519}. JWT
are self-contained, and contain digital signatures, making them comparable in
the abstract to X.509 certificates (section \ref{sec:related:certs}).

\subsection{Distributed Identifiers}
This document focuses on authorization, which must follow authentication.
Either involves a degree of identification of the authenticated and authorized
subject. The W3C Distributed Identifier (DID)\cite{w3cDID} specification is of
some interest here, as it describes a system whereby some amount of
authentication can be achieved without requiring a server component.

This is done by a similar method as X.509 certificates, in that the DID
document may contain public key material. Unlike X.509, however, DID documents
can specify to a limited degree what purpose such public keys serve.

The
relationship between public key and additional material is also inverted: where
X.509 uses the public key as the implied identifier and associates further
material with it, DIDs are themselves identifiers. They may refer to public
keys, however, establishing a similar link.

It is necessary to stress that for this reason, DID documents are not self-verifiable.
Rather, the specification requires a verifiable storage medium.

\subsection{Seals and Notaries}
\label{sec:related:historical}

From a historical perspective, authorization has had two major forms dating
back millennia.

People recognized that rulers can not be omnipresent, so resorted to
delegation. In order to transfer to the ruler's authority to their delegates,
systems often put into places provided delegates with difficult to reproduce
seals of authority. We still rely on this mechanism in the form of e.g. police
badges or similar means of establishing sanctioned delegation of authority.

In a more generalized form, we refer to this as mechanism as "power of
attorney", a concept which can be brought into the digital realm via
cryptographic signatures \cite{PoA}.

The underlying mechanism to this is notarization, in which an established
authority signs statements that then delegate trust, establishing the rules by
which cryptographic certificates function.

\subsection{Evaluation}
\label{sec:related:eval}

While far from a complete listing of related work, the above selection serves
to highlight the common characteristics of related technologies that together
define the context in which distributed authorization must operate.

\begin{enumerate}
  \item Several technologies attempt to encode \Grants/ into
    transmittable tokens (OCAP, ICAP, OAuth, JWT).
  \item Cryptographic identification can be solved by associating some
    identifier with identifying metadata, and signing the combination with a
    trusted key (X.509, PGP, JWT).
  \item Authentication and authorization tend to be provided by an \AuthServer/
    or local service (OCAP, ICAP, OAuth).
  \item Nonetheless, some limited support for embedding \Grants/ into
    cryptographically verifiable assertions relating to identifiers (PGP, JWT,
    DID).
\end{enumerate}

As distributed authorization requires that the use of an \AuthServer/ as a
centralizing single point of failure is eliminated, the focus of this work is to
establish a generalized, flexible means of authorization using an approach
comparable to, but expanded from PGP, JWT and DID. The result is a fully self-verifiable
capability for use in distributed systems.
