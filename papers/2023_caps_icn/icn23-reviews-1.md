ICN 2023 Paper #1 Reviews and Comments
===========================================================================
Paper #1 Capabilities for Distributed Authorization in Information-Centric
Networking


Review #1A
===========================================================================

Overall merit
-------------
3. Weak accept

Reviewer expertise
------------------
3. Knowledgeable

Paper summary
-------------
The paper lays out an architecture for distributed authorization, which has authentication as a precondition.  The presentation is couched in terms of *capabilities*, i.e., blobs that convey authorization according to the relevant trust structure, using cryptographic mechanisms.  The goal of the architecture is to remove any central points of failure, such as authorization servers.
The main point of the paper seems to be that the *authorization decision*, i.e., the answer to the question whether a certain action by a certain principal is permitted, can be pre-made, cryptographically sealed, and conveyed through a "time-delayed channel" - in a manner analogous to what public-key certificates do (sometimes) for authentication.

Comments for authors
--------------------
The paper is well-written, and the observation that the authorization decision ("Is this <action> by <principal> authorized?") can be made in advance and cryptographically signed was useful, at least to me.  However, the paper would be improved by relating this observation, and especially the description in Section 3.6, to what is done in practice today, in particular with the protocols mentioned earlier in Section 2.3.   What exactly is the salient difference between the proposed capabilities and what is done today in OATH 2.0 or JWT?  And what do the differences say about the security of today's Web?

I do not agree with the statement in Section 4 that "The NDN trust model revolves around encryption of Data packets, so that only parties legitimately issuing Interests should be able to make use of the embedded payload."  That depends on your point of view.  For the issuer of an Interest (the so-called "consumer"), it would seem that the "trust model" is more about who is authorized to produce content with the given name - i.e., what key is trusted to sign the desired content.   After reading Section 4 several times, I'm still not entirely sure what problem is being solved.  Enabling "NDN routers to determine whether an Interest should be satisfied" has been the subject of a good deal of research.  I agree that capabilities could be an interesting approach to that problem, but also agree that you don't have room to cover it in this short paper.  The suggestion would be to position your proposal with respect to prior work on those problems (e.g., Interest-Key Binding Rule, self-certifying names, etc.). Is there an assumption here that the "router" and the application have a common trust anchor?

In some sense, the mechanism for authorization is just like any other means of policy enforcement, and the "decision" is indeed made in advance, at policy-formulation time.  The important thing is how the rules of the policy are encoded and related to identifiers that can be parsed and "mapped" to application protocol elements and/or real-world principals, so that any entity with the complete policy and context can determine whether authorization is warranted.

What is the relationship between this work and the work of Nichols et al (see previous work on Data-Centric Trust Toolkit, and the "Defined Trust Transport" Internet-Draft)?  Or the "Delegation Logic" of [1]?

[1] N. Li, B. Grosof and J. Feigenbaum, "Delegation Logic: A Logic-Based Approach to Distributed Authorization", ACM Transactions on Information and System Security, 6(1), Feb 2003.



Review #1B
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
4. Expert

Paper summary
-------------
In the paper ``Capabilities for Distributed Authorization in Information-Centric Networking,'' the authors describe the general setting of distributed authorization, and explore challenges for authorization in ICNs.  The authors identify challenges that exist in granting authorization and verifying it in both time and topologically distributed settings.  This paper proposes the use of capabilities to address complexities of distributed authorization.

Comments for authors
--------------------
he paper has a very long lead up to its actual proposal, but when discussion begins on page 4/5, the paper seems to be suggesting a model that closely matches how X.509 currently works.   The authors could enhance the writing to explain how this approach specifically differs from that existing solution.  Additionally, the second evaluation section (Section 4) does not seem to relay any specific evaluation results.  It leaves the reader to wonder if this approach has been tried, and what the results of using it may be.

Strengths:
+ The authors present reasoning about how authorization can logically be decomposed into two phases: creating and verification.

+ The authors outline some of the risks of centralization of this process.


Weaknesses:
- The paper has a large number of structural challenges and typos.  Among these is the fact that the actual, specific, proposal of the paper is not discussed until about Section 3.2, or more specifically until 3.6 (on page 5 of 6).  This leaves the reader unaware of the specific proposal until very late in the paper.

- There were numerous places in the paper where additional citations would need to be added.  As just one example, OCAP is mentioned multiple times but never explained or cited.

- Another concern is that the authors do not clearly differentiate this work from the basic nature and protections of X.509 certificates.  As the paper reads, the proposed approach seems to envision an approach that matches they way X.509 currently
works.  It was not immediately clear how this proposal differs in a substantive way.

- The descriptions of how X.509 and the Web PKI work did not clearly match my understanding of those protocols/standards/communities.  Though the latter was not explicitly mentioned, it is distinct, and at times the paper seemed to conflate them and draw conclusions that do not match my understandings.  Additionally, concepts like trust-stores were not mentioned in descriptions of certificate chain verification and CAs, in Section 2.2.  In the context of that section, trust-stores (perhaps) explain some missing elements.

- Similar to the above, the authors discuss PGP, but do not discuss the Web of Trust, which is a distinct topic to the protections of PGP.  It would be helpful to the reader to have a more fulsome discussion that distinguishes these (as the paper makes use of the trust level), which has more to do with the Web of Trust than just PGP, in my view.

- The work of this paper sounds very similar (though clearly not exactly the same) as how Kerberos works.  It seems like that architecture should be included in the discussion and the reader would benefit from seeing how it differs from this proposal.


- The writing style made the paper slightly challenging to read, with many paragraphs that were only one sentence long.



Review #1C
===========================================================================

Overall merit
-------------
3. Weak accept

Reviewer expertise
------------------
3. Knowledgeable

Paper summary
-------------
This paper proposes to extend NDN with capabilities. The majority of the paper is designing the capability system itself. Only in the last page does the paper discuss, in a limited fashion, how the proposed capabilities might be added to NDN.

Comments for authors
--------------------
•	I find this to be a very interesting idea. The general concept of capabilities is probably well-matched to an ICN, especially as you have noted because of the elimination of centralization of authentication and authorization. That said, as I will discuss below I think you need to review the issue of revocation.

•	I think you have slightly misrepresented the Dennis and van Horn paper.  After a quick reread of the paper, I think that if a process presents a capability, it isn’t necessarily the case that the user who owns the process is the principal. The underlying concept of a capability is that a principal can delegate access to another principal, so the principal of the user owning the process presenting any particular capability may have received the capability as a delegation from the actual owner. This is exactly the situation in which the capability has an “N” tag (nonowner) rather than a “O” (owner) tag. I don’t think this is a major issue for you, but it bears further clarification.

•	You proposed that authentication is a necessary first step for authorization. That is not true for all the work on attribute-based authorization. There is significant and important work in that area. Your work is based on prior authentication and a great deal of other work is, but not all.

•	I have several concerns about revocations. First, in Section 3.5, revocations must derive and come from the delegation source, unlike the access capabilities presented by the agent that wants to actually gain access. Thus, the source of a revocation needs to understand how to deliver all the relevant revocations to the provider of the information for validation. This gets to my second point. In NDN, this is extremely challenging and can’t be ignored. Because a key element of NDN is the CS capabilities, the provider of the access can be anywhere and everywhere in the network. That means that if a delegator of a capability wants to issue a revocation, it must somehow make sure that that revocation information is distributed to anywhere the retrieving interest packet might go.



Review #1D
===========================================================================

Overall merit
-------------
1. Reject

Reviewer expertise
------------------
4. Expert

Paper summary
-------------
The paper argues for a capabilites-based authorization mechanisms. It analyzes existing authorization schemes and it explains why they are not suitable for ICN.

Comments for authors
--------------------
The paper re-invents the wheel and it neglects years of related work. Capabilities-based access control is a well studied topic (e.g., [1],[2]). Moreover, there exist many efforts studying methods for representing capabilities, including Macaroons [3], ZCAPs [4], and of course verifiable credentials (https://www.w3.org/TR/vc-data-model/). Some of these approaches have even been studied in the context of ICN. Capabilities-based solutions are also excessively studied in the context of secure file systems (see for example Tahoe-LAFS https://en.wikipedia.org/wiki/Tahoe-LAFS)  Related to that the paper invents new terminology which is unrelated to the established terminology (e.g., Grantor instead of Issuer). 

Moreover, the proposed approach, i.e., including capabilities in an interest packet, is completely insecure, since it is susceptible to replay attacks. The paper considers a solution to this problem as future work. 

Overall, although the paper is nicely motivated, it does not make any obvious contribution in this area. 

Minor
- Add a space beeten a reference and the previous word (e.g., more variations[6] should be more variation [6])
- Section 2.4, the technology is called decentralized identifiers and anot distributed identifiers

[1] S. Gusmeroli, S. Piccione, and D. Rotondi, “A capability-based security
approach to manage access control in the internet of things,” Mathematical and Computer Modelling, vol. 58, no. 5, pp. 1189–1205, 2013.

[2] J. L. Hernandez-Ramos, A. J. Jara, L. Marin, and A. F. Skarmeta, ´
“Distributed capability-based access control for the internet of things,”
Journal of Internet Services and Information Security (JISIS), vol. 3, no.
3/4, pp. 1–16, 2013.

[3] A. Birgisson, J. G. Politz, Ulfar Erlingsson, A. Taly, M. Vrable, ´
and M. Lentczner, “Macaroons: Cookies with contextual caveats for
decentralized authorization in the cloud,” in Network and Distributed
System Security Symposium, 2014

[4] C. L. Webber, M. Sporny Eds. (2020) Authorization capabilities for
linked data. [Online]. Available: https://w3c-ccg.github.io/zcap-ld/



Review #1E
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
2. Some familiarity

Paper summary
-------------
This paper notes that current authorization approaches for distributed systems frequently introduce centralization to the architecture and explores cryptographic capabilities as a means to remove the authorization server as a single point of failure.

Comments for authors
--------------------
I found this paper interesting and engaging up until section 4 that introduced a hasty and flawed sort of model for distributed authorization in NDN. If instead, you had taken your "generalized concept for distributed authorization" and perhaps applied it to some other published approaches in ICN to note where centralization is introduced and/or might be avoided, I would find it worthy of publication. Indeed, I thought you were trying to provide a sort of conceptual framework for distributed authorization that could be helpful for ICN right up until section 4. In that respect, this could be valuable work for the ICN community.

A few nits:
sec 2.1 Third para, 1st sentence: I think "are" should be replaced by "have been"

sec 3.3 4th para: this needs better explanation
              5th para: I appreciate the brevity, but I think some sentence or phrase of introduction of OBJECT, PRIVILEGES as used here is needed

sec 4 Not going to go into detail here, but this seems pretty flawed. One thing among many is the odd use of Interests which seem to be employed to carry content essentially.



Review #1F
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
4. Expert

Paper summary
-------------
This paper discusses a high-level design and its most basic requirements to grant capabilities in NDN.

Comments for authors
--------------------
Capabilities, access tokens, passkeys etc. are all well established and studied concepts in the field of security research with many implementations and evaluations in the past. In that regard, the described high-level design of a grantor providing a signed capability to a grantee is far from being novel.

However, the idea of providing access control in NDN though capabilities integrated in interest messages would be interesting to research further and compare/contrast to encryption based access control in terms of DoS protection, performance and security under various threat scenarios (especially in enterprise level or IoT level NDN networks). I wish the authors had done more in laying out a more detailed design as well as an evaluation on its security and performance especially in comparison to encryption based access control for me to be able to advocate its acceptance.
