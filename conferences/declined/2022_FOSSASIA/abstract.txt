In 2000, Roy Fielding published his dissertation on Representational State Transfer. Fielding had been actively working on the HTTP standards, which were guiding by informal design principles that REST formalizes. The talk will revisit this architectural style to disambiguate it from how the REST term has become applied since.

Much as software freedoms exist, we need to address "internet user freedoms", and their relationship to human rights. Existing internet technology must be evaluated in this light, in how it supports or hinders human rights.

In the following section, benefits and shortcomings in the REST architectural style will be analyzed, with regards to the previous discussion as well as technical requirements. We will explore potential solutions to the issues raised, and their effect on the overall architectural style. This must include some mention of best practices and workarounds in wide use today.

The talk will also provide background on concrete work already done or underway on this and adjacent topics.

Finally, we'd like to briefly introduce the non-profit organization created to support work on this project. If we are to create a better digital world, we'll need patience and all the help we can get.

