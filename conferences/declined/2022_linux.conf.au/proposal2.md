The Interpeer Project intends to build a future human centric Internet.

It's also trying very hard not to fail - so a software architecture made up of
composable and re-usable parts that permit for exploration of ideas takes
precedence over rushing to completion.

The project has so far yielded a number of software repositories, with many
more to follow. The talk is an introduction to the different components, their
role within the overarching project. It will cover design philosophy, lead
through example applications, and provide an introduction to the project file
system structure(s).

Not so flatteringly, it'll also discuss how some decisions have room for
improvement - or maybe your contribution?

The language is C++, and some understanding multithreading is expected. But
the talk also includes some high-level concepts on network I/O and protocol
design, so is fairly friendly to beginners in that subject area.

This talk is intended to be somewhat complementary to the protocol design
discussions at https://reset.substack.com so that would-be contributors can
find their way around the project(s) more easily.
