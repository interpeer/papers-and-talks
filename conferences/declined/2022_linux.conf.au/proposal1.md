The Internet today isn't what we wanted it to be.

In the 90s, we thought the Internet would be this thing that empowers people.
The great equalizer that doesn't care about any variable in your background,
where only a passion for improving the world would suffice to enact change.

This has been happening, and still is. But increasingly, it is made more
difficult.

The old elites have been partially replaced by the tech barons that came from
the nerds of yesteryear, who rode this tech wave to success. But to what end? To
exercise the same kind of control as existed before.

The Internet has shifted power structures for sure, but the great equalization
hasn't happened.

It's easy to blame this on power corrupting - but it may also have to do with
factoring power into the Internet architecture to begin with, and thus into
everything built on top of it.

The Internet Protocol stack is based on hierarchical network topologies - as
the name implies, distribution of control ever more central points is part of
the design. The Web started with wrong assumptions about the roles of its
participants, and left crucial parts undefined which are most easily patched
by centralisation. Blockchain at best promises a new oligarchy of miners.

Can we be more radical than that, and create a substructure for communications
that does not nudge its users towards one form of power distribution or the
other?

That is what a truly human centric Internet should look like. It's also the
vision of the Interpeer Project.

This talk will lightly explore some of the history of computing and the
internet, as well as current use cases, with an eye towards how specific
developments changed the landscape, with the intent of charting out a loose
set of requirements for a future, human centric internet.
