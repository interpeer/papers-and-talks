# Vorstellung des Ideengeberteams bzw. des Ideengebenden

Interpeer gUG ist eine gemeinnützige Gesellschaft die Forschung & Entwicklung in zukünftigen Internettechnologien betreibt. Hierbei sollen heutige und zukünftige Probleme mit der aktuellen Internet-Architektur angegangen werden, um langfristigen Mehrwert zu schaffen.

# Titel

Interpeer - Europäisches Information-Centric Networking

# Bitte stellen Sie Ihre Idee und die damit gelöste Problemstellung kurz vor.

Digitale Souveränität ist aktuelles politisches Thema, und beschreibt "die Fähigkeiten und Möglichkeiten von Individuen und Institutionen, ihre Rolle(n) in der digitalen Welt selbstständig, selbstbestimmt und sicher ausüben zu können" (BMI). Zukünftige Internet-Technologien werden jedoch im aussereuropäischen Ausland entwickelt, ohne grundlegende Europäische Werte zu berücksichtigen. Ein Europäischer Gegenvorschlag kann jetzt dafür sorgen, diese Werte für morgen zu sichern.

# Beschreiben Sie Ihre Idee, Ihr Konzept oder Ihr Modell und wie es das zuvor beschriebene Problem löst.

Die Grundidee ist, einen Europäischen Entwurf für eine zukünftige Internet-Architektur zu entwickeln, um die Zukunftsgestaltung nicht einzig aussereuropäischen Regierungen zu überlassen. Dabei sollten neben zivilen und öffentlichen Anwendungsfällen selbstverständlich auch militärische berücksichtigt werden.

**Derzeitige Situation**

Standards für das Internet werden international verhandelt. Allerdings entstammen Vorschläge für zukünftige Standards zunächst Firmeninteressen.  Blickt man weiter in die Zukunft, kommen Vorschläge aus der Forschung und Entwicklung im Auftrag aussereuropäischer Regierungen, die den Europäischen Blickwinkel auf Digitale Souveränität nicht ausreichend berücksichtigen.  Somit besteht die Gefahr, dass die Deutsche Perspektive auf zukünftige Standards erst zu spät in diese Prozesse einfliesst, und nicht ausreichend umgesetzt werden kann. Wir sehen das Resultat davon, von genügend unterschiedlichen Positionen auszugehen, in den zähen Verhandlungen zu internationalem Datentransfer.

Die Verhandlungen der Politik bereiten die Standards der nächsten Jahre vor, die allesamt auf der aktuellen Internet-Architektur aufbauen.  Daneben schreitet allerdings die Forschung und Entwicklung an der nächsten Internet-Architektur weitestgehend unbeachtet voran, und wird nahezu ausschliesslich von unseren Verhandlungspartnern vorangetrieben. Die Vermutung, dass diese ihre aktuelle Position in der Technologie verankern, wird durch Veröffentlichungen zum Thema stetig untermauert.

Die Wahrscheinlichkeit ist hoch, dass die nächste Internet-Architektur sich dem Konzept des Information-Centric Networking (ICN) annähert, in dem Applikationen primär Inhalte direkt addressieren anstatt der Maschine, auf der die Inhalte vorliegen. Dies bedeutet auch, die Inhalte besser auf redundante Maschinen verteilt werden können, und sorgt somit für erhöhte Ausfallsicherheit.

Die Folge davon ist allerdings, dass sich das Sicherheitskonzept verschiebt: anstatt Verbindungen zu sichern, ist vielmehr von Bedeutung, die Inhalte selber zu schützen. Dafür müssen Sicherheits- und Datenschutzkonzepte viel tiefer im Technologiestack verankert werden.

Bei den aktuellen Forschungsansätzen ist dies jedoch nicht der Fall. Ebenso werden hier Anwendungen in der Echtzeitkommunikation nicht besonders behandelt.  Da diese Forschung im Ausland mit hohen Geldsummen gefördert wird, ist allerdings abzusehen, dass zukünftige Vorschläge zu Internet-Standards hierauf aufbauen, und die gleichen Sicherheitsprobleme mit sich bringen werden, wie wir sie derzeit sehen.

Grundsätzlich besteht noch ausreichend Zeit, hierauf Einfluss zu nehmen. Dies erfordert allerdings eigene Forschung und Entwicklung: hierdurch kann sowohl frühzeitig Einfluss auf den Dialog in der Forschung eingenommen werden.  Darüberhinaus bietet ein Gegenvorschlag auch die Möglichkeit, in der Entwicklung Vorreiter zu werden.

**Information-Centric Networking**

Der Vorteil von ICN besteht hauptsächlich darin, daß die Loslösung von Inhalten vom Speicherort Optimierungen im Datenfluss und Ausfallsicherheit bieten kann. Paul Baran beschrieb in den 1960ern zentralisierte, dezentrale und verteilte Netzwerkarchitekturen um zu dem Schluss zu kommen, dass letztere den grössten Schutz davor bieten, dass der Ausfall einzelner Kommunikationsknoten ganze Teile des Kommunikationsnetzes mit sich reißt. Seine Forschung bezog sich auf bewusste Zerstörung solcher Knoten im Konfliktfall - beeinflusste allerdings die Gestaltung des ARPANET und somit das heutige Internet.

Während die niedrigeren Schichten des Internet immer noch weitestgehend diesen Prinzipien folgen, sind die höheren, den Anwendungen näher liegenden Schichten, heutzutage oft weitestgehend zentralisiert. Der Hintergrund ist hier u.A. auch der, dass sich Sicherheitsanforderungen mit bisheriger Technologie oft einfacher zentral steuern liessen. Insbesondere entstammen heutige Sicherheitskonzepte einer Zeit, in der Verschlüsselung sich nicht effizient mit CPUs umsetzen liess, die dabei noch genügend verbrauchsarm waren, um mobil eingesetzt werden zu können; entsprechende Überlegungen beeinflussen die Lösungsfindung noch in den 90er Jahren - also in der Zeit, in der das Internet an Bedeutung gewann.

Da dieses Problem nicht mehr aktuell ist, wird es an der Zeit, mit Blick auf die Zukunft die Internet-Architektur neu zu evaluieren, und sich auch wieder auf die ursprünglichen Entwurfskriterien zu besinnen. Eine ICN-Lösung, die Sicherheit im Kern verbaut, ist genau der Lösungsansatz der moderne Sicherheitslösungen mit der Ausfallsicherheit eines verteilten Kommunikationsnetzes verbinden kann.

Interpeer gUG arbeitet seit einiger Zeit an entsprechenden Ansätzen im zivilen Einsatzbereich. Es werden hierbei auch Anforderungen berücksichtigt, die dem Anwendungsbereich der Echtzeitkommunikation mit (kommerziellen) Drohnen entstammen, um Beyond Visual Line of Sight (BVLOS) bzw. Remotely Piloted Aircraft Systems (RPAS) sicher zu ermöglichen. Jedoch sind die grundlegenden Kriterien - Ausfallsicherheit, Echtzeitfähigkeit und Datensicherheit - auch im öffentlichen und militärischen Bereich höchst relevant.

**ISO-OSI Modell**

ICN-Lösungen werden im Forschungsbereich oftmals auf die bestehende Internet-Infrastruktur überlagert; dies bietet Vorteile dabei, diese Lösungen grossflächig zu testen, und später nahtlos auszurollen. Allerdings gehen hierbei teilweise Informationen zwischen den verschiedenen Abstraktionsschichten verloren, was Optimierungen entgegenwirken kann.

Zum Beispiel ist bei Videoübertragung oftmals wichtiger, mehr Bilder in niedriger Qualität zu liefern, damit Anwender dem Verlauf besser folgen können. Video-Codecs sind generell nicht darauf ausgelegt, die Qualität ständig anzupassen, aber entsprechende Experimente waren erfolgreich.  Allerdings fehlt schlichtweg die Information aus der Transportschicht, mit welcher Bitrate Bilder derzeit bzw. vorrausschauend gesendet werden können, um solche Qualitätsanpassungen sinnvoll vornehmen zu können.

Auch wenn Abstraktionsschichten im Netzwerk sinnvoll sind, um Komplexität besser handhaben zu können, bietet ein höherer Informationsaustausch zwischen den Schichten Möglichkeiten zur Optimierung wie der obigen. Interpeer forscht daher auch gezielt daran, wie Anwendungsanforderungen sich durch die Schichten ziehen können.

Bei ICN ist dies insbesondere von Vorteil, da die Unabhängigkeit von speziellen Kommunikationsknoten auch bedeutet, dass Datenflüsse über mehrere Pfade redundant und/oder komplimentär erfolgen können. Das Ende-zu-Ende-Prinzip im Design grossflächiger Netzwerksysteme besagt allerdings auch, dass Kernfunktionalität um bspw. solche Pfade zu steuern nicht auf einen Anwendungsfall reduziert werden sollte, sondern vielmehr gemeinsame Prinzipien vielfältiger Anwendungsfälle herausgearbeitet werden sollten. Dies reduziert die Komplexität von Kommunikationsknoten, und trägt auch so zur Ausfallsicherheit bei.

**Neue Herausforderungen**

Aktuell ist das Metaverse in aller Munde, sowie die Frage, inwieweit diese zukünftige Trends im Internet beeinflussen wird. Zwar handelt es sich hier nur um ein Produkt einer einzelnen Firma. Das Konzept eines weltweiten Netzes aus drei-dimensionalen virtuellen Räumen existiert allerdings seit Dekaden, und es wird immer wieder spekuliert, inwieweit die Existenz dieser Technologie die Gesellschaft beeinflusst. Meta's Vorstoss hier muss nicht mit diesem Produkt zu tun haben um zu zeigen, dass der Stand der Technik jetzt weit genug fortgeschritten ist, um solch ein Projekt ernsthaft anzugehen.

Beim Metaverse handelt es sich um Virtuelle Realität (VR). Auch Erweiterte Realität (augmented/expanded reality, AR) bietet Nährstoff für Spekulationen.  Wo VR allerdings derzeit zum grössten Teil in Form von Videospielen zum Einsatz kommt, zieht AR seit einigen Jahren in die Industrie ein. Einige der hier verwandten Techniken gehen auf vorige Entwicklungen im militärischen Einsatzbereich zurück; man kann AR als modernisierte Variante eines herkömmlichen Head-Up Displays (HUD) verstehen.

Gegenüber solchen HUDs verbindet AR mit VR allerdings, dass deutlich grössere Datenmengen benötigt werden, um das Sichtfeld mit relevanten Informationen zu erweitern. Der Knackpunkt hierbei ist, dass diese Daten sich mit jeder Bewegung von einem konzeptionellen Raum in einen anderen vollständig ändern können.  Dabei ist die Bewegung des "Piloten" genauso relevant wie die anderer Teilnehmer.

Somit kann kaum gewährleistet sein, dass die momentan notwendigen Daten bereits auf einem Gerät vorliegen; vielmehr müssen sie in Echtzeit nachgeladen werden.  Der Netzwerkverkehr ist hier also im äussersten Masse von Spitzenlast dominiert.  Aus Netzwerksicht ist dies primär ein Routing-Problem: wie werden Daten mit möglichst geringer Latenz und hohem Übertragungsvolumen an die beteiligten Endgeräte übertragen?

ICN bietet hier den Vorteil, dass es von Grund auf darauf optimiert ist, Daten mit Endgeräten zu verknüpfen, und weniger Endgeräte untereinander. Dadurch sind Daten die (auch partiell) bei einem Teilnehmer vorliegen auch zeitnah für nahestende Teilnehmer verfügbar.

Hierfür müssen allerdings verschiedene Komponenten zahnlos ineinander greifen: Routing muss für lokales Verteilen optimiert werden können. Transportprotokolle müssen bei partiellen Daten weiterarbeiten, denn auch solche Teildaten können den entscheidenden Unterschied bringen. Zudem müssen Daten vertraulich versendet werden können.

**Technische Komponenten**

Bei diesem Projekt handelt es sich zu einem gewissen Teil um Grundlagenforschung.  Zwar baut jede Komponente auf Erfahrung mit der bestehenden Internet-Architektur bzw. anderen ICN-Vorschlägen auf. Andererseits ist an den obigen Ausführungen ersichtlich, dass einige Bereiche analysiert und überarbeitet gehören, um einen sicheren und zeitgemäss performanten Gesamtvorschlag auszuarbeiten.

Somit ist der Überblick über die notwendigen Komponenten hinreichend bekannt.  Davon sind allerdings nicht alle vollständig entwickelt, insbesondere im Hinblick auf Einsatzbereitschaft.

**Akteure / Konvergenz**

Im Laufe der weiteren Forschung & Entwicklung ist es besonders interessant, weitere Anwendungsfälle zu verstehen, um die Lösung wenn möglich entsprechend anzupassen. Hier fehlt insbesondere Erfahrung im militärischen Einsatzbereich.

Um dies zu ermöglichen wäre insbesondere von Interesse, bei dem Entwurf von zukünftigen technischen Standards mit der Bundeswehr zu kooperieren. Auch Konvergenz mit bestehenden Systemen kann so gewährleistet werden.

# Bitte erläutern Sie ausführlich die Problemstellung, die mit Ihrer Idee gelöst wird.

Deutsche und Europäische Interessen werden bei den Entwürfen zu zukünftigen Internet-Architekturen derzeit nicht ausreichend berücksichtigt. Das birgt die Gefahr, dass die Interessen unserer Bürger und Institutionen hier nicht ausreichend vertreten sind, wozu auch die Interessen der Streitkräfte zählen.

In anderen Bereichen passt sich Europa oft internationalem Geschehen an (beispielsweise ist abzusehen, dass EASA sich bei den Standards zur Identifikation von Drohnen an die FAA angleichen wird, da der Europäische Einfluss auf diese Prozesse derzeit gering ist).

Während dies je nach Problemstellung durchaus die sinnvollste Lösung sein kann, ist die Situation bei ICN kritischer: insbesondere sind Cybersicherheit und Echtzeitanwendungen bei bisheriger Forschung im Ausland nicht überzeugend berücksichtigt. Beides hat allerdings einen hohen Stellenwert insbesondere im militärischen Einsatzbereich.

Es gilt also, frühzeitiger Einfluss zu nehmen. Da es sich bei Forschung und Entwicklung nicht um konkrete Umsetzungsunterfangen handelt, ist dies am ehesten möglich, indem ein Gegenentwurf entwickelt und international auf einschlägigen Konferenzen und in Standardisierungsgremien präsentiert wird.

Dadurch werden zwei positivere Szenarien ermöglicht: Im Idealfall kann sich der Gegenvorschlag durchsetzen. Falls nicht, können entsprechende Anregungen aus dem Gegenvorschlag sich durchsetzen, die besonders in unserem Interesse liegen.

Ohne diesen Diskurs aufzunehmen steht jedoch auch hier bevor, dass Europa sich schlicht den anderswo getroffenen Entscheidungen angleichen muss, um weiterhin grenzübergreifende Kommunikation zu erhalten.

# Worin besteht der spezifische Mehrwert der Einführung Ihrer Idee gegenüber etablierten Konzepten?

Wie im Ansatz schon oben angeführt, lösen die bisherigen Ansätze zwei Dinge in nicht zufrieden stellender Weise.

**Cybersicherheit**

Im ISO/OSI-Modell wird relativ streng in Schichten gehandelt; niedrigere Schichten stellen einen Kommunikationsweg bspw. zwischen zwei Funkmodulen her, während höhere Schichten dafür sorgen, dass Datenpakete von Knoten zu Knoten weitergereicht werden, bis sie ihr Ziel erreichen. Dabei ist für jeden "Sprung" von einem Knoten zum anderen weitestgehend irrelevant, wie die Daten in Funkwellen oder elektrischen Signalen im Kabel kodiert werden.

ICN stellt hier einen neuen Ansatz dar: in der Andwendung werden Daten nicht mehr für einen spezifischen Knoten addressiert. Stattdessen fordern einzelne Knoten einen Datensatz über einen Identifikator an, und verschiedene Knoten können darauf antworten. Das kann die Datenübertragung deutlich optimieren.

Dadurch ist nicht mehr von Interesse, ob ein Knoten dafür authorisiert ist, diesen Datensatz zu liefern. Vielmehr gilt es sicherzustellen, dass der Datensatz selber nicht mitgelesen wurde und authentisch ist.

ICN erfordert daher, Cybersicherheitskonzepte, die sonst eher der Anwendung überlassen sind, in tiefere Kommunikationsschichten einzubringen. Dies ist bei bisherigen Forschungsansätzen nicht deutlich der Fall.

Wir haben bereits verteilte Authorisierungskonzepte (CAProck) sowie Kapselungsformate (Vessel) ausgearbeitet, die diesen Anforderungen standhalten.  Mindestens sollten diese Ansatze in andere ICN-Lösungen einfliessen, um Cybersicherheit ausreichend zu gewährleisten. Darüberhinaus haben wir ein Transportprotokoll (Channeler) entwickelt, dass es erleichtert, partielle Daten auszuliefern, falls dies niedrige Latenz gewährleisten kann. Zudem arbeiten wir an Vorschlägen zu Routing-Standards (ROSA), die Teilnehmer in dem Netzwerk schneller zu optimalen Orten für angefragte Daten führt.

Verwandt mit diesem Thema ist die Frage, wie Daten in einem verteilten System sicher gelöscht werden können. Dies ist ein weitesgehend ungelöstes Problem sowohl in der bisherigen Internet-Architektur wie auch bei ICN. Es handelt sich hier auch um ein besonders schwieriges Problem.

Während eine finale Antwort zu der Frage aussteht, haben wir auch hier Vorschläge entwickelt, wie ein ICN-System hier bessere Lösungen bieten kann, als es bisher der Fall ist.

**Echtzeitanwendungen**

Andere ICN-Lösungen schlagen einen relativ puristischen Weg ein, um grössere Daten zu verteilen. Zunächst werden grössere Inhalte in kleinere Teile zerlegt. Danach erhält jedes Teil einen eigenen Identifikator. Dann gehen diese Ansätze jeweils unterschiedliche Wege; es bleibt allen jedoch gleich, dass der gesamte Datensatz als geordnete Liste aller Teile identifiziert wird.

Hieraus ergeben sich zwei Probleme bei Echzeitanwendungen:

1. Jeder Teil-Datensatz muss separat angefordert werden, was in der Auslieferung zu höherer Latenz durch zusätlich notwendige Nachrichten führt.
2. Bei jeder Veränderung des Datensatzes - wie bei Echtzeitandwendungen wie Streaming dauernd der Fall ist - muss mindestens ein Teil Liste neu synchronisiert werden. Dies verändert den Identifikator der Liste, weswegen der erste Punkt oben auch hier greift.

Unser Vorschlag geht vielmehr davon aus, dass sich Ressourcen ständig ändern.  Sollte ein Dokument tatsächlich statisch bleiben ist dies der Sonderfall, der nur dazu führt, dass hier *nichts* getan werden muss.

Dazu kapseln wir Daten in einen streamingfähigen Container, der auch notwendige Authorisierungsinformationen erhält und Ende-zu-Ende verschlüsselt sein kann.  Das Übertragunsprotokoll konzentriert sich dann darauf, nicht Datenteile einzeln anzufordern, sondern (Teilbereiche) des Containers. Im Idealfall kann so eine einzelne Anfrage einen ganzen Datenstrom übertragen.

# Welche technischen, organisatorischen und rechtlichen Hindernisse wären für eine schnelle Umsetzung zu überwinden?

Interpeer gUG ist eine gemeinnützige Forschungseinrichtung, die laut ihrer Satzung öffentlich zugängliche Ergebnisse erzielen muss. Dadurch ist jedes Forschungsergebnis zunächst auch im militärischen Anwendungsbereich einsetzbar.

Wir sind in der Lage, nach Bedarf und Finanzierungslage weiteres Personal einzustellen, um dieses Projekt weiter voran zu treiben.

Bei der Kooperation zu zukünftigen Standards gilt, etwaige sicherheitsrelevanten Szenarien genügend abstrakt in die Dokumente einfliessen zu lassen, um weiterhin öffentlichen Zugang gewährleisten zu können.

Andererseits ist dem Unternehmen durchaus möglich, in kleinerem Bereich Auftragsforschung durchzuführen, bspw. um spezielle, nicht zu veröffentlichende Teilbereiche auszuarbeiten. Der Hauptteil der Einkünfte sollte aber, um die
Gemeinnützigkeit zu gewähren, aus Spenden usw. bestehen.

Nach Vorbereitung von Standards ist der eigentliche Standardisierungsprozess nur im Gremium durchzuführen, das allerdings vergleichsweise klein sein kann.  Hier ist nötig, dass andere Institutionen durch ihre Vertreter am Prozess
teilnehmen.
