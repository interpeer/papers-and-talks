#!/usr/bin/env python

def elem_iterator(elem, indent = 0):
  yield indent, elem
  if hasattr(elem, 'children'):
    for x in elem.children:
      for entry in elem_iterator(x, indent + 1):
        yield entry


def raw_text_combined(elem):
  from marko.inline import RawText
  if not isinstance(elem, RawText):
    return None
  text = ''
  for x in elem.children:
    text += str(x)
  return text


def display(root):
  from marko.inline import RawText

  for indent, elem in elem_iterator(root):
    text = str(elem)
    if isinstance(elem, RawText):
      text = '[combined] %s' % raw_text_combined(elem)
    print(' ' * (2 * indent), text)


def filter_title(root):
  import re
  pat = re.compile('conference', re.IGNORECASE)

  from marko.inline import Link, RawText
  nextlink = False
  for indent, elem in elem_iterator(root):
    if isinstance(elem, RawText):
      if pat.match(raw_text_combined(elem)):
        nextlink = True

    if isinstance(elem, Link) and nextlink:
      nextlink = False
      # display(elem)
      return raw_text_combined(elem.children[0])


def parse(text):
  import marko
  doc = marko.parse(text)
  # display(doc)
  title = filter_title(doc)

  # Extract & expand on year
  import re
  pat = re.compile('(?P<year>[0-9]+)')
  m = pat.search(title)
  year = m.groupdict()['year']
  if len(year) != 2 and len(year) != 4:
    raise RuntimeError('Strange year length: %d - %s' % (len(year), year))
  if len(year) == 2:
    year = '20' + year

  return year, title


def process_file(filename):
  with open(filename, 'r') as fh:
    contents = fh.read()
    year, title = parse(contents)

    # Category is any path component preceding a path that starts with the year
    import os.path
    psplit = filename.split(os.path.sep)
    cat = None
    for i, p in enumerate(psplit):
      if p.startswith(year):
        if i > 0:
          cat = psplit[i - 1]
        break
    if cat is None:
      cat = 'current'
    return cat, year, title, filename


def collect():
  collected = {}

  import glob
  for path in glob.glob('**/conference.md', recursive=True):
    category, year, title, filename = process_file(path)

    if category not in collected:
      collected[category] = {}
    if year not in collected[category]:
      collected[category][year] = set()
    collected[category][year].add((title, filename))

  return collected

def print_year(year, data):
  print(year)
  print('-' * len(year))
  print()

  for entry in sorted(data):
    print('- [%s](./%s)' % entry)

  print()


def print_section(title, data):
  print(title)
  print('=' * len(title))
  print()

  for year in sorted(data.keys(), reverse=True):
    print_year(year, data[year])


def print_formatted(data):
  print_section('Current', data.get('current', {}))
  print_section('Past', data.get('done', {}))
  print_section('Declined', data.get('declined', {}))


if __name__ == '__main__':
  confdata = collect()
  print_formatted(confdata)
