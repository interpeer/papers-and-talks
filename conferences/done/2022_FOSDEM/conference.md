- Conference: [FOSDEM '22](https://fosdem.org)
- Schedule Entry: [On the Far side of REST](https://fosdem.org/2022/schedule/event/rfc_rest_future/)
- Video and presentation links on schedule page.
