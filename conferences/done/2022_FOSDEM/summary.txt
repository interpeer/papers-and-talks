REST, the architecture underlying the web's protocols, has proven its benefits in creating a globe-spanning, decentralized information network. However, REST is showing its age - it was designed when surveillance capitalism, identity theft, information warfare, and other threats were largely hypothetical concerns. Unavoidably, REST leaves many of these issues unaddressed. Best practices fill some gaps, but may not be universally adopted.

The Interpeer Project has been awarded a grant from the Internet Society Foundation for research and development into a next generation architecture that addresses current and future Internet user needs. Such an architecture needs to embrace the strengths of REST, incorporate known best practices, but ideally make worst practices impossible.

This talk presents the issues with REST in some detail and lays out proposed solution sketches. The ideal is to invite participation, however. The 'net needs a wide range of view points to be fixed.
